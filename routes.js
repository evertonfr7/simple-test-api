const express = require("express");

const routes = express.Router();

//autentication route
routes.post("/login", (req, res) => {
    const { login, password } = req.body;

    if(login === 'root' && password === 'root'){
        res.json({
            isValid: true,
            token: 'hash-hipotetica',
            user: {
                namef: 'SmartBeeAdmin',
            }
        })
    }else{
        res.send(401).json({
            error: 'Not Authorized'
        })
    }
});

routes.get('/', (req, res) => {
    res.json({
        serverOpened: true
    })
})

routes.get('/apiaries', (req, res) => {
    res.json({
        apiaries: [
            {
                name: 'Colmeia 01',
                isActive: true
            },
            {
                name: 'Colmeia 2',
                isActive: true
            },
            {
                name: 'Colmeia 3',
                isActive: false
            },
            {
                name: 'Colmeia 4',
                isActive: true
            },
            {
                name: 'Colmeia 5',
                isActive: false
            }
        ]
    })
})

routes.get('/apiary', (req, res) => {
    res.json({
        apiary: {
            temperature: 35.4,
            humidity: 45.2,
            co2: 25.1,
            o2: null,
            noise: 45.0,
            weight: null,

        }
    })
})

routes.get('/', (req, res) => {
    res.json({
        serverOpened: true
    })
})

module.exports = routes;
