const express = require('express');
const cors = require('cors');
const routes = require('./routes');

const app = express();

app.use(cors());
app.use(express.json());

app.use(routes);

try {
    app.listen(3000, () => {
        console.log("Server started...");
    });
} catch (error) {
    console.log('Não foi possível iniciar o servidor.');
}